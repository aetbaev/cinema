<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFilmshowsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('filmshows', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('film_id');
            $table->integer('moviehall_id');
            $table->integer('price');
            $table->timestamp('start');

            //$table->foreign('film_id')->references('id')->on('films');
            //$table->foreign('moviehall_id')->references('id')->on('moviehalls');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('filmshows');
    }
}
