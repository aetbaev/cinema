<?php

use Illuminate\Database\Seeder;

class FilmsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // фильмы
        DB::table('films')->insert([
            [
                'name' => 'Сделано в Америке'
            ],
            [
                'name' => 'Kingsman: Золотое кольцо'
            ],
            [
                'name' => 'Бегущий по лезвию 2049'
            ]]);

        // залы
        DB::table('moviehalls')->insert([
            [
                'id' => 1,
                'name' => 'Зал 1'
            ],
            [
                'id' => 2,
                'name' => 'Зал 2'
            ]]);

        // заполним залы местами
        for ($hall_id = 1; $hall_id <= DB::table('moviehalls')->count(); $hall_id++) {
            for ($row = 1; $row <= 5; $row++) {
                for ($place = 1; $place <= 10; $place++) {
                    DB::table('moviehall_places')->insert(
                        [
                            'moviehall_id' => $hall_id,
                            'row' => $row,
                            'place' => $place
                        ]);
                }
            }
        }

        // сеансы на 2 дня вперед во все залы

        $films_ids = DB::table('films')->get()->pluck('id')->toArray();
        $moviehalls = DB::table('moviehalls')->get();

        $hours = ['11:00', '15:00', '19:00'];

        for ($day = 0; $day <= 2; $day++) {
            foreach ($hours as $hour) {
                foreach ($moviehalls as $hall) {
                    // случайныq фильм без проверки дубля в соседнем зале
                    DB::table('filmshows')->insert([
                        'film_id' => $films_ids[array_rand($films_ids)],
                        'moviehall_id' => $hall->id,
                        'start' => date('Y-m-d', strtotime('+' . $day . ' day'))  . ' ' . $hour,
                        'price' => 200
                    ]);
                }
            }
        }


    }
}
