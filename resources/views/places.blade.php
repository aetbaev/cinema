{{ csrf_field() }}
<input type="hidden" name="filmshow_id" value="{{$filmshow_id}}">

<table>
@foreach($places as $k => $v)
    <tr>
        <td>ряд {{$k}}</td>
        @foreach($v as $kk => $vv)
            <td><a href="#" class="btn @if($vv['free']) btn-light @else btn-danger @endif"><input style="display: none" type="checkbox" name="place[]" value="{{$vv['id']}}">{{$kk}}</a></td>
        @endforeach
    </tr>
@endforeach
</table>