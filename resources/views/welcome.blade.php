<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Cinema</title>

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>

    </head>
    <body>
        <div class="container">
            @foreach($list as $v)
            <div>
                <h4>{{$v['film']->name}}</h4>
                @foreach($v['hours'] as $h)
                    <a href="/places/{{$h['id']}}" class="btn btn-light btn-place">{{$h['hour']}}</a>
                @endforeach
            </div>
            <hr />
            @endforeach
        </div>

        <script>
            $(function() {
                $('.btn-place').on('click', function(e) {
                    e.preventDefault();
                    $('#exampleModal .modal-body').load(this.href, function() {
                        $('#exampleModal').modal('show');
                    })
                });

                $('#exampleModal .modal-body').on('click', '.btn', function(e) {
                    e.preventDefault();
                    if ($(this).is('.btn-danger')) {
                        return;
                    }
                    if ($(this).is('.btn-light')) {
                        $(this).removeClass('btn-light').addClass('btn-success')
                    } else {
                        $(this).removeClass('btn-success').addClass('btn-light')
                    }
                    $(this).children('input').prop('checked', $(this).is('.btn-success'));
                });

                $('#order').submit(function() {
                    if (!$('#order input:checked').length) {
                        alert('Не выбраны места');
                        return false;
                    }
                });
            })
        </script>

        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <form action="/order" method="post" id="order">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Places</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary" id="btn-order">Order</button>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </body>
</html>
