<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Moviehalls extends Model
{
    protected $table = 'moviehalls';

    /**
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function places()
    {
        return $this->hasMany('App\MoviehallPlaces', 'moviehall_id', 'id');
    }
}
