<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MoviehallPlaces extends Model
{
    protected $table = 'moviehall_places';
}
