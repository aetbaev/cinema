<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Filmshows extends Model
{
    protected $table = 'filmshows';


    /**
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function film()
    {
        return $this->belongsTo('App\Films', 'film_id', 'id');

    }

    /**
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function moviehall()
    {
        return $this->belongsTo('App\Moviehalls', 'moviehall_id', 'id');
    }

}
