<?php

namespace App\Http\Controllers;

use App\Filmshows;
use App\Tickets;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class IndexController extends Controller
{
    public function index() {

        // shows today
        $list = Filmshows::whereDate('start', date('Y-m-d'))
            ->orderBy('start', 'asc')
            ->get();

        // prepare
        $result = [];
        foreach ($list as $v) {
            if (!isset($result[$v->film->id]['film'])) {
                $result[$v->film->id]['film'] = $v->film;
            }

            $result[$v->film->id]['hours'][] = [
                'id' => $v->id,
                'price' => $v->price,
                'hour' => date('H:i', strtotime($v->start))
            ];
        }
        unset($list);

        return view('welcome', ['list' => $result]);
    }


    /**
     * Места на сеанс
     * @param $id сеанс
     */
    public function places($id) {

        // информация по сеансу
        $show = Filmshows::find($id);

        // купленные места
        $places_sold = Tickets::where('filmshow_id', $id)->get()->pluck('place_id')->toArray();

        // все места
        $places = $show->moviehall->places->toArray();
        $result = [];
        foreach ($places as $v) {
            $result[$v['row']][$v['place']] = [
                'id' => $v['id'],
                'free' => !in_array($v['id'], $places_sold) /* помечаем купленные места */
            ];
        }
        unset($places);

        return view('places', ['places' => $result, 'filmshow_id' => $id]);
    }


    /**
     * Покупка билета
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function order(Request $request) {

        $filmshow_id = $request->post('filmshow_id');
        $places = $request->post('place');
        foreach ($places as $place_id) {
            // создание билета
            $ticket = new Tickets();
            $ticket->filmshow_id = $filmshow_id;
            $ticket->place_id = $place_id;
            $ticket->save();
        }

        return view('order');
    }
}
